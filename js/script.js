var actualizaciones = 0;
function cargar_clima() {

    var clima = new XMLHttpRequest();
    var ciudad = 'Santiago'

    clima.open('GET', 'http://api.wunderground.com/api/e069d13c9432ad2f/conditions/q/CL/'+ciudad+'.json', false);
    clima.send(null);

    var datos = JSON.parse(clima.response);

   var ciudad = datos.current_observation.display_location.full;
   var temperatura = datos.current_observation.temp_c;
   var humedad = datos.current_observation.relative_humidity;
   var icon = datos.current_observation.icon_url;
   var clima = datos.current_observation.weather;


    $('#ubicacion').html(ciudad);
    $('#temperatura').html(temperatura);
    $('#humedad').html(humedad);
    $('#icon').attr("src", icon);  //En este caso, por tratarse de una imagen, se manipula el atributo src del elemento

    


    var bgcolor = '#FFF';

   switch(clima){
       case 'Fog':
           bgcolor= '#F7F4EA';
        break
        case 'Clear':
         bgcolor= '#BFD7EA'
         break
    }

    $('#container').css('background-color',bgcolor)
}

$(document).ready(function(){
    cargar_clima();
    setInterval(cargar_clima, 3600000); //actualiza el tiempo cada 1 hora.
});